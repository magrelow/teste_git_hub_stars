//
//  ViewPresenterTest.swift
//  test_git_api_starsTests
//
//  Created by Eric Soares Filho on 22/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import XCTest
@testable import test_git_api_stars

class ViewPresenterTest: XCTestCase {

    var sut = ViewPresenter()
    
    func testExample() throws {
        sut.gitInfrastructure = GitInfrastructureMock()
        
        let expectationTest = expectation(description: "retrieved")
        sut.getListGitStars{ (response, error) -> Void in

            XCTAssertEqual(response.count > 0, true)
            XCTAssertEqual(error == .noError, true)
            expectationTest.fulfill()
        }
        
        waitForExpectations(timeout: 1000)
    }

}
