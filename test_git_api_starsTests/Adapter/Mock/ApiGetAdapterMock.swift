//
//  ApiGetAdapterMock.swift
//  test_git_api_starsTests
//
//  Created by Eric Soares Filho on 22/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
@testable import test_git_api_stars

class ApiGetAdapterMock: ApiGetAdapterProtocol {
    func getSimpleApi(url: String, completionHandler: @escaping(Data?, AdapterErro) -> Void) {
        let data = "{\"total_count\": 741158,\"incomplete_results\": false,\"items\": [{\"id\": 21700699}]}".data(using: .utf8)!
        completionHandler(data, .noError)
    }
}



