//
//  GitInfrastructureMock.swift
//  test_git_api_starsTests
//
//  Created by Eric Soares Filho on 22/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
@testable import test_git_api_stars

class GitInfrastructureMock: GitInfrastructureProtocol {
    func getListStars(completitionHandler: @escaping (GitResponse?, InfraError) -> Void) {
        
        var gitResponse = GitResponse()
        
        gitResponse.total_count = 1
        gitResponse.items = []
        gitResponse.items?.append(GitItemResponse(id: 2,
                                                  owner: GitItemOwnerResponse(login: "Login",
                                                                              avatar_url: "https://avatars2.githubusercontent.com/u/484656?v=4"),
                                                  full_name: "teste/teste",
                                                  score: 2.0)
                                    )
        
        
        completitionHandler(gitResponse, .noError)
    }
}
