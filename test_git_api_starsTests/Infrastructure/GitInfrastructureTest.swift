//
//  GitInfrastructureTest.swift
//  test_git_api_starsTests
//
//  Created by Eric Soares Filho on 22/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import XCTest
@testable import test_git_api_stars

class GitInfrastructureTest: XCTestCase {
    let sut = GitInfrastructure()
    
    func testingJsonCorrect() throws {
        sut.apiGetAdapter = ApiGetAdapterMock()

        let expectationTest = expectation(description: "retrieved")
        sut.getListStars(){ (response, error) -> Void in

            XCTAssertEqual(response != nil, true)
            XCTAssertEqual(error == .noError, true)
            expectationTest.fulfill()
        }
        
        waitForExpectations(timeout: 1000)
    }
}
