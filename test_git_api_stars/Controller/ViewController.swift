//
//  ViewController.swift
//  test_git_api_stars
//
//  Created by Eric Soares Filho on 21/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import UIKit

class ViewController: UIViewController  {
    
    var listOfGit: [ViewModel] = []
    @IBOutlet weak var tableview: UITableView!
    var refreshControl = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initFront()
        self.initFrontData()
    }
    
    func initFront() {
        self.tableview.delegate = self
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableview.addSubview(refreshControl)
    }
    
    func initFrontData() {
        ViewPresenter().getListGitStars()
        { [weak self] (result, error) -> Void in
            if error == .noError {
                self?.listOfGit = result
                DispatchQueue.main.async {
                    self?.tableview.reloadData()
                    self!.refreshControl.endRefreshing()
                }
            } else {
                
                DispatchQueue.main.async {
                    self!.alertMessage()
                    self!.refreshControl.endRefreshing()
                }
            }
        }
    }
    
    func alertMessage () {
        let alert = UIAlertController(title: "Erro", message: "Existiu um erro, gostaria de tentar novamente?", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { action in
            self.initFrontData()
        }))
        alert.addAction(UIAlertAction(title: "Não", style: .cancel, handler: nil))

        self.present(alert, animated: true)
    }
    
}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfGit.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! ListOfGitsTableViewCell
        cell.repoNameLabel.text = listOfGit[indexPath.row].repoName
        cell.avatarImageView.image = listOfGit[indexPath.row].image
        cell.starCountLabel.text = String(listOfGit[indexPath.row].numberStar!)
        cell.nameLabel.text = listOfGit[indexPath.row].name
        return cell
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.initFrontData()
    }
}

