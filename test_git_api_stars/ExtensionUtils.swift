//
//  ExtensionUtils.swift
//  test_git_api_stars
//
//  Created by Eric Soares Filho on 21/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    public func getImageFromURL(urlString:String) -> UIImage {
        let url = URL(string: urlString)
        let data = try? Data(contentsOf: url!)

        if let imageData = data {
            return UIImage(data: imageData)!
        } else {
            return UIImage()
        }
    }
}
