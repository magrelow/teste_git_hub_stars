//
//  ViewPresenter.swift
//  test_git_api_stars
//
//  Created by Eric Soares Filho on 21/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

class ViewPresenter {
    
    var gitInfrastructure: GitInfrastructureProtocol = GitInfrastructure()
    
    func getListGitStars (completionHandler: @escaping ([ViewModel], PresenterError) -> Void){
        gitInfrastructure.getListStars(){ (response, error) -> Void in
            var viewModelList: [ViewModel] = []
            
            if error == .noError {
                response?.items!.forEach{
                    viewModelList.append(
                        ViewModel(name: ($0.owner?.login)!,
                                  repoName: $0.full_name!,
                                  numberStar: $0.score!,
                                  imageUrl: ($0.owner?.avatar_url)!)
                    )
                }
                completionHandler(viewModelList, .noError)

            } else {
                completionHandler(viewModelList, .genericError)
            }
        }
    }
}


