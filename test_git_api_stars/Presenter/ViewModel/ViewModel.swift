//
//  ViewModel.swift
//  test_git_api_stars
//
//  Created by Eric Soares Filho on 21/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation
import UIKit

struct ViewModel {
    var name: String?
    var repoName: String?
    var numberStar: Double?
    var imageUrl: String?
    var image = UIImage()
    
    init(name: String, repoName: String, numberStar: Double, imageUrl: String) {
        self.name = name
        self.repoName = repoName
        self.numberStar = numberStar
        self.imageUrl = imageUrl
        self.image = image.getImageFromURL(urlString: imageUrl)
    }
}
