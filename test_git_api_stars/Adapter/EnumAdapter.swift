//
//  EnumAdapter.swift
//  test_git_api_stars
//
//  Created by Eric Soares Filho on 21/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

enum AdapterErro {
    case genericError
    case imageNilError
    case imageDownloadError
    case noError
}
