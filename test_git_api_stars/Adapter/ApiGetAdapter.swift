//
//  ApiGetAdapter.swift
//  test_git_api_stars
//
//  Created by Eric Soares Filho on 21/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

protocol ApiGetAdapterProtocol {
    func getSimpleApi(url: String, completionHandler: @escaping(Data?, AdapterErro) -> Void)
    
}

class ApiGetAdapter: ApiGetAdapterProtocol {
    
    func getSimpleApi(url: String, completionHandler: @escaping(Data?, AdapterErro) -> Void) {
        let urlString = url
        if let url = URL(string: urlString)
        {
            URLSession.shared.dataTask(with: url){ data, res, err in
                if let data = data {
                    completionHandler(data, .noError)
                } else {
                    completionHandler(nil, .genericError)
                }
            }.resume()
        }
    }
}



