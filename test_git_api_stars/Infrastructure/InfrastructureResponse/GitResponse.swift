//
//  GitResponse.swift
//  test_git_api_stars
//
//  Created by Eric Soares Filho on 21/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

struct GitResponse: Decodable {
    var total_count: Int?
    var items: [GitItemResponse]?
    
    enum CodingKeys: String, CodingKey {
        case total_count
        case items
    }
}


struct GitItemResponse: Decodable {
    var id:Int?
    var owner: GitItemOwnerResponse?
    var full_name: String?
    var score: Double?
    
    enum CodingKeys: String, CodingKey {
        case id
        case owner
        case full_name
        case score
    }
    
}


struct GitItemOwnerResponse: Decodable {
    var login:String?
    var avatar_url: String?
    
    enum CodingKeys: String, CodingKey {
        case login
        case avatar_url
    }
}
