//
//  EnumInfrastructure.swift
//  test_git_api_stars
//
//  Created by Eric Soares Filho on 21/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

enum InfraError {
    case convertionJsonError
    case genericError
    case noError
}
