//
//  GitInfastructure.swift
//  test_git_api_stars
//
//  Created by Eric Soares Filho on 21/08/20.
//  Copyright © 2020 erimia. All rights reserved.
//

import Foundation

protocol GitInfrastructureProtocol {
    func getListStars(completitionHandler: @escaping (GitResponse?, InfraError) -> Void)
}

class GitInfrastructure: GitInfrastructureProtocol {
    var apiGetAdapter: ApiGetAdapterProtocol = ApiGetAdapter()
    
    func getListStars(completitionHandler: @escaping (GitResponse?, InfraError) -> Void) {
        
        apiGetAdapter.getSimpleApi(url: "https://api.github.com/search/repositories?q=language:swift&sort=stars")
        { (resultData, error) -> Void in
            if error == .noError {
                let decoder = JSONDecoder()
                
                do {
                    let gitResponse = try decoder.decode(GitResponse.self, from: resultData!)
                    completitionHandler(gitResponse, .noError)
                } catch {
                    completitionHandler(nil, .convertionJsonError)
                }
            } else {
                completitionHandler(nil, .genericError)
            }
        }
    }
}

